
//################################################################
// C implementation of k Nearest Neighbours brute force algorithm
//################################################################
//################################################################

#include <stdio.h>
#include <time.h>   //need the time to generate psuedoranom numbers
#include <stdlib.h> //header file for rand() function
#include <math.h>   //need this for rounding and perhaps other maths...

#include <omp.h>    //parallel openMP

//****Pototypes of functions****
float drand ( float low, float high );
float calcEucDist_n_d(float* a, float* b, int dim);
void display_Array (float **arr,int m, int n);
void matrixTranspose(float *arr,int m,int n);
void display_Array_3D (float *arr,int m, int n,int dim);

//#quick sort#
//serial
int partition(int p, int r, float *data);
void seq_qsort(int p, int r, float *data);
void q_sort(int p, int r, float *data, int low_limit);
void validate_sort(int n, float *data);
//Parallel
int partition_PAR(int p, int r, float *data);
void seq_qsort_PAR(int p, int r, float *data);
void q_sort_PAR(int p, int r, float *data, int low_limit);
void validate_sort_PAR(int n, float *data);


//*bubble sort*
void arrBubbleSort (float **arr, int m, int n);
void arrBubbleSort_PAR (float **arr, int m, int n);

//*bitonic search*
#define SWAP(x,y) temp = x; x = y; y = temp;
//serial
void compare(float *data, int i, int j, int dir);
void bitonicmerge(float *data,int low, int len, int dir);
void recbitonic(float *data, int low, int len, int dir);
void bitonic_Sort(float *data, int len);
//parallel
void compare_PAR(float *data, int i, int j, int dir);
void bitonicmerge_PAR(float *data,int low, int len, int dir);
void recbitonic_PAR(float *data, int low, int len, int dir);
void bitonic_Sort_PAR(float *data, int len);


//set OMP environment variables?


int main()
{

int test_variation1 = 3;
int test_variation2 = 3;
    int dim_Arr[3]= {128,256,512};
    int n_Arr[3]= {200,400,800};

    int test_num = 1;
    float combined_Time_Dist;     //combined time / test_num = average time
    float combined_Time_Dist_PAR;

    float combined_Time_qSort;
    float combined_Time_qSort_PAR;

    float combined_Time_bSort;
    float combined_Time_bSort_PAR;

    float combined_Time_bitSort;
    float combined_Time_bitSort_PAR;

  for (int i = 0; i < test_variation1; i++) {
  for (int j = 0; j < test_variation2; j++) {
  for (size_t k = 0; k < test_num ; k++) {


printf("\n######################################");
printf("\n   ### Variation %d   \n",i+j);
printf("\n   ###    Test %lu     \n",k);
printf("#######################################\n");

  //testing values
  printf("\nTESTING VALUES:\n");
  int dim= dim_Arr[i];   printf("\nDimensions: \t\t%d\n",dim);
  int m=100000; printf("Reference points: \t%d\n",m); //p --- reference -> I need to change these to p & q?
  int n= n_Arr[j];    printf("Query points: \t\t%d \n",n); //q --- query
  int k=1;      //printf("k-value: \t\ŧ %d \n\n",k);

  printf("\n\nCalculating... \n" );

//################### Initialize Data Arrays #######################
  // Initialize an an array of pointers -> https://www.geeksforgeeks.org/dynamically-allocate-2d-array-c/
  //float p_ref[m][dim];
  float t1,t2;

  float **p_ref = malloc(sizeof(float*)*m);
  for (size_t i = 0; i < m; i++) {
    p_ref[i] = malloc(sizeof(float)*dim);
  }
   float **q_query = malloc(sizeof(float*)*n);
   for (size_t i = 0; i < n; i++) {
     q_query[i] = malloc(sizeof(float)*dim);
   }


   //################### Generating pseudo-random data ###################
  srand(time(NULL));   // Initialize rand() funtion, should only be called once.
  t1 = omp_get_wtime();
  //#pragma omp parallel sections //-> twice as slow as serial
  //  #pragma omp section
  for (int i = 0; i<(m); i++){
    for (int j = 0; j < dim; j++){
      p_ref[i][j] = drand(5,1);  //can vary range if one likes
      //probe printf("\np_ref [%d][%d] = %f ",i,j, p_ref[i][j]);
    }
  }
    //#pragma omp section
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < dim; j++) {
      *(*(q_query +i)+j) = drand(2,1);   //The derefeference *(*(q_query +i)+j) and q_query[i][j] are equivalent and refer to an array of pointers
      //probe printf("\nq_query[%d][%d] = %f ",i,j, q_query[i][j]);
    }
  }

  t2 = omp_get_wtime();
  //printf("\nTime to generate random data in PARALLEL: %f",t2-t1);
//probe  display_Array(p_ref,m,dim);
//probe display_Array(q_query,n,dim);


  //-----------------------------------------------------------------------------
  // ################# SERIAL #################
  //###########################################

  //################# Distance Calculation #################

  //timer
  float eucDist_Time;
  t1 = omp_get_wtime();

  //float eucDistMatrix[m][n];
  float **eucDistMatrix;
  eucDistMatrix = malloc(sizeof(float*)*m);      //$$ check to see if this m should be n
  for (size_t i = 0; i < m; i++) {                //$$ check to see if this m should be n
    eucDistMatrix[i] = malloc(sizeof(float)*n);  //$$ check to see if this n should be m
  }
  //access each point in array
  for (size_t j = 0; j < n; j++) {    //cycle through all rows of q_query
    for (size_t i = 0; i < m; i++) {  //cycle through all rows of p_ref
      float eucDist = 0;
        eucDist = calcEucDist_n_d(*((q_query+j)),p_ref[i],dim); //performing calculation
        eucDistMatrix[i][j] = eucDist;  //capturing each distance independantly for sorting
    }
  }
  t2 = omp_get_wtime();
  eucDist_Time = t2-t1;
  printf("The time taken for euclidean distance calculation in SERIAL was: %f s\n",eucDist_Time );
  combined_Time_Dist += eucDist_Time;

    //################# QUICK SORT #################
    t1 = omp_get_wtime();
    //probe printf("\nQuick Sort BEFORE:\n");display_Array(eucDistMatrix,m,n);
    for (size_t i = 0; i < m; i++) {
      q_sort(0,n-1,eucDistMatrix[i],0);
    }
    t2 = omp_get_wtime();
    float quickSort_Time = t2-t1;
    combined_Time_qSort += quickSort_Time;
    printf("The time taken for the Quick Sort in SERIAL was: %f s\n",quickSort_Time);
     //probe printf("\nQuick Sort AFTER:\n");display_Array(eucDistMatrix,m,n);
     //probe validate_sort(m,eucDistMatrix[0]);

    //################# BUBBLE SORT #################
    //probe printf("\nBubble Sort BEFORE:\n");display_Array(eucDistMatrix,m,n);
    t1 = omp_get_wtime();
    arrBubbleSort(eucDistMatrix, m,n);
    t2 = omp_get_wtime();
    float bubbleSort_Time = t2-t1;
    combined_Time_bSort += bubbleSort_Time;
    printf("The time taken for the Bubble Sort in SERIAL was: %f s\n",bubbleSort_Time);
    //printf("\nBubble Sort AFTER:\n");probe display_Array(eucDistMatrix,m,n);
    //probe validate_sort(m,eucDistMatrix[0]);

    //################# BITONIC SORT #################
    //probe printf("\nBitonic Sort BEFORE:\n"); display_Array(eucDistMatrix_PAR,m,n);

    t1 = omp_get_wtime();
    for (size_t i = 0; i < m; i++) {
      bitonic_Sort(eucDistMatrix[i],n);
    }
    t2 = omp_get_wtime();
    float bitonic_Time = t2-t1;
    combined_Time_bitSort += bitonic_Time;
    printf("The time taken for the Bitonic Sort in SERIAL was: %f s\n",bitonic_Time);
    //probe printf("\nBitonic Sort AFTER:\n"); display_Array(eucDistMatrix_PAR,m,n);
    //probe validate_sort(m,eucDistMatrix[0])


    //free(eucDistMatrix);
    printf("\n\n");
  //################# PARALLEL#################
  //###########################################

    //################# Distance Calculation #################
    //timer
    float eucDist_Time_PAR;
    t1 = omp_get_wtime();

    //float eucDistMatrix_PAR[m][n];
    float **eucDistMatrix_PAR;
    eucDistMatrix_PAR = malloc(sizeof(float*)*m);
    for (size_t i = 0; i < m; i++) {
      eucDistMatrix_PAR[i] = malloc(sizeof(float*)*n);
    }

    #pragma omp parallel for // num_threads(8) //schedule (dynamic)    //dynamic schedule did not improve speeds
    for (size_t j = 0; j < n; j++) {      //cycle through all rows of q_query
      #pragma omp parallel for
      for (size_t i = 0; i < m; i++) {    //cycle through all rows of p_ref
        float eucDist = 0;
          eucDist = calcEucDist_n_d(*(q_query+j),p_ref[i],dim);
          eucDistMatrix_PAR[i][j] = eucDist; //capturing each distance independantly for sorting
      }
    }
    t2 = omp_get_wtime();
    eucDist_Time_PAR = t2-t1;
    printf("The time taken for euclidean distance calculation in PARALLEL was: %f s\n",eucDist_Time_PAR);
    combined_Time_Dist_PAR += eucDist_Time_PAR;

    //################# QUICK SORT #################
    t1 = omp_get_wtime();
    //probe printf("\nQuick Sort BEFORE:\n");display_Array(eucDistMatrix,m,n);
    #pragma omp parallel for num_threads(8)
      for (size_t i = 0; i < m; i++) {
      q_sort_PAR(0,n-1,eucDistMatrix_PAR[i],0);
    }
    t2 = omp_get_wtime();
    float quickSort_Time_PAR = t2-t1;
    combined_Time_qSort_PAR += quickSort_Time_PAR;
    printf("The time taken for the Quick Sort in PARALLEL was: %f s\n",quickSort_Time_PAR);

    //################# BUBBLE SORT #################
    //probe printf("\nBubble Sort BEFORE:\n"); display_Array(eucDistMatrix_PAR,m,n);
    t1 = omp_get_wtime();
    arrBubbleSort_PAR(eucDistMatrix_PAR, m,n);
    t2 = omp_get_wtime();
    float bubbleSort_Time_PAR = t2-t1;
    combined_Time_bSort_PAR += bubbleSort_Time;
    printf("The time taken for the Bubble Sort in PARALLEL was: %f s\n",bubbleSort_Time_PAR);
    //probe printf("\nBubble Sort AFTER:\n"); display_Array(eucDistMatrix_PAR,m,n);
    //probe validate_sort(m,eucDistMatrix[0]);

    //################# BITONIC SORT #################
    //probe printf("\nBitonic Sort BEFORE:\n"); display_Array(eucDistMatrix_PAR,m,n);
    t1 = omp_get_wtime();
    #pragma omp parallel for num_threads(8)
    for (size_t i = 0; i < m; i++) {
      bitonic_Sort_PAR(eucDistMatrix_PAR[i],n);
    }
    t2 = omp_get_wtime();
    float bitonic_Time_PAR = t2-t1;
    combined_Time_bitSort_PAR += bitonic_Time_PAR;
    printf("The time taken for the Bitonic Sort in PARALLEL was: %f s\n",bitonic_Time_PAR);
    //probe printf("\nBitonic Sort AFTER:\n"); display_Array(eucDistMatrix_PAR,m,n);
    //probe validate_sort(m,eucDistMatrix[0])

} //test_num

} //test_variation1
} //test_variation2



  return 0;
  //######################## end of main() ###################################
  //##########################################################################
}

//*************************************************
//******************* FUNCTIONS *******************
//*************************************************


//################# Distance Calculation #################
// making pseudo-random numbers as float datatype
float drand ( float low, float high ){
    return ( (float)rand() * ( high - low ) ) / (float)RAND_MAX + low;
}

// calculating the distance
float calcEucDist_n_d (float* a, float* b,int dim){
  float c_sqrd = 0;
  //#pragma omp parallel for
  for (size_t i = 0; i < dim; i++) {
    c_sqrd += pow((a[i]-b[i]),2); //potential race conditions to update
  }
  float c = sqrt(c_sqrd);
  return c;
  }

//################# BUBBLE SORT #################
void arrBubbleSort (float **arr, int m, int n) {
  //float sortArr[m][n];
  float **sortArr;
  sortArr = malloc(sizeof(float*)*m);
  for (size_t i = 0; i < m; i++) {
    sortArr[i] = malloc(sizeof(float)*n);
  }
  for (size_t i = 0; i < m; i++) {
    //Initialize row i for sorting
    for (size_t j = 0; j < n; j++) {
      sortArr[i][j] = *(*(arr+i)+j);
      sortArr[i][j+1] = *(*(arr+i)+j+1);
    }
    int flag = 0;
    while (flag < n) {
    for (size_t j = 0; j < n-1; j++) {
        if (sortArr[i][j]>sortArr[i][j+1]) {
          float high = sortArr[i][j];
          float low = sortArr[i][j+1];
          //swap
          sortArr[i][j] = low;
          sortArr[i][j+1] = high;
          //update array in main()
          *(*(arr+i)+j) = sortArr[i][j];
          *(*(arr+i)+j+1) = sortArr[i][j+1];
        }
        else {
        }}
      flag ++;
    }
  }}

  void arrBubbleSort_PAR (float **arr, int m, int n) {
    //float sortArr[m][n];
    float **sortArr;
    sortArr = malloc(sizeof(float*)*m);
    #pragma omp parallel for num_threads(4) //schedule (dynamic)
    for (size_t i = 0; i < m; i++) {
      sortArr[i] = malloc(sizeof(float)*n);
    }
    #pragma omp parallel for num_threads(4)
    for (size_t i = 0; i < m; i++) {
      //Initialize row i for sorting
      for (size_t j = 0; j < n; j++) {
        sortArr[i][j] = *(*(arr+i)+j);
        sortArr[i][j+1] = *(*(arr+i)+j+1);
      }
      int flag = 0;
      while (flag < n) {
      for (size_t j = 0; j < n-1; j++) {
          if (sortArr[i][j]>sortArr[i][j+1]) {
            float high = sortArr[i][j];
            float low = sortArr[i][j+1];
            //swap
            sortArr[i][j] = low;
            sortArr[i][j+1] = high;
            //update array in main()
            *(*(arr+i)+j) = sortArr[i][j];
            *(*(arr+i)+j+1) = sortArr[i][j+1];
          }
          else {
          }}
        flag ++;
      }
    }}




//################# QUICK SORT #################
//### SERIAL ###
int partition(int p, int r, float *data){
	float x=data[p];int k=p; int l=r+1;float t;
	while(1){
		do{k++;}
		while((data[k] <= x) && (k<r));
		do{l--;} while(data[l] > x);
			while(k<l){
				t=data[k];
				data[k]=data[l];
				data[l]=t;
				do{k++;} while(data[k]<=x);
			do{l--;} while(data[l]>x);
		}
		t=data[p];
		data[p]=data[l];
		data[l]=t;
		return l;
	}
}

void seq_qsort(int p, int r, float *data){
	if(p < r){
		int q=partition(p, r, data);
		seq_qsort(p, q-1, data);
		seq_qsort(q+1, r, data);
	}
}

void q_sort(int p, int r, float *data, int low_limit){
	if(p<r){
		if((r-p)<low_limit){
			seq_qsort(p,r,data);
		}
		else{
			int q=partition(p,r,data);
			q_sort(p,q-1,data,low_limit);
			q_sort(q+1,r,data,low_limit);
		}
	}
}

void validate_sort(int n, float *data){
	int i;
	for(i=0;i<n-1;i++){
		if(data[i] > data[i+1]){
			printf("Validate failed. \n");
		}
	}
	printf("Validate passed.\n");
}


//### PARALLEL ###
int partition_PAR(int p, int r, float *data){
	float x=data[p];int k=p; int l=r+1;float t;
  while(1){
		do{k++;}
		while((data[k] <= x) && (k<r));
		do{l--;} while(data[l] > x);
			while(k<l){
				t=data[k];
				data[k]=data[l];
				data[l]=t;
				do{k++;} while(data[k]<=x);
			do{l--;} while(data[l]>x);
		}
		t=data[p];
		data[p]=data[l];
		data[l]=t;
		return l;
	}
}

void seq_qsort_PAR(int p, int r, float *data){
	if(p < r){
		int q=partition_PAR(p, r, data);
		seq_qsort_PAR(p, q-1, data);
		seq_qsort_PAR(q+1, r, data);
	}
}

void q_sort_PAR(int p, int r, float *data, int low_limit){
	if(p<r){                     //low < high
		if((r-p)<low_limit){
			seq_qsort_PAR(p,r,data);
		}
		else{
			int q=partition_PAR(p,r,data);
      //(1)#pragma omp parallel sections    //much slower than serial - increased overhead??
      {
      //(1)#pragma omp section
      //(2)#pragma omp task shared(data) firstprivate(p,q,low_limit) // slower than serial
      // try only do the number log the number of cores 4 ->2
      //
			{q_sort_PAR(p,q-1,data,low_limit);}
      //(1)#pragma omp section
      //(2)#pragma omp task shared(data) firstprivate(r,q,low_limit)
      {q_sort_PAR(q+1,r,data,low_limit);}
      //(2)#pragma omp taskwait
      }
		}
	}
}

void validate_sort_PAR(int n, float *data){
	int i;
	for(i=0;i<n-1;i++){
		if(data[i] > data[i+1]){
			printf("Validate failed. \n");
		}
	}
	printf("Validate passed.\n");
}

//########## BITONIC SEARCH ##########
 //compare and swap based on dir (direction)
 void compare(float *data, int i, int j, int dir)
{
    int temp;
    if (dir == (data[i] > data[j]))
    {
        SWAP(data[i], data[j]);
    }
}

//Sorts a bitonic sequence in ascending order if dir=1
//otherwise in descending order
void bitonicmerge(float *data,int low, int len, int dir)
{
    int k, i;     // k = split length
    if (len > 1)
    {
         k = len / 2;
         for (i = low;i < low+k ;i++)
            compare(data, i, i+k, dir);
            bitonicmerge(data,low, k, dir);
            bitonicmerge(data,low+k, k, dir);
    }
}

//Generates bitonic sequence by sorting recursively
//two halves of the array in opposite sorting orders
//bitonicmerge will merge the resultant data
void recbitonic(float *data, int low, int len,int dir)
{
    int up = 1;
    int down = 0;

    int k;
    if (len > 1)
    {
        k = len / 2;
        recbitonic(data,low, k, up);
        recbitonic(data,low + k, k, down);
        bitonicmerge(data,low, len, dir);
    }
}

//Sorts the entire array
void bitonic_Sort(float *data,int len)
{
    int up = 1;

    recbitonic(data,0, len,up);
}

//### PARALLEL ###
//(1)https://www.cs.rutgers.edu/~venugopa/parallel_summer2012/bitonic_openmp.html

//compare and swap based on dir (direction)
void compare_PAR(float *data, int i, int j, int dir)
{
   int temp;
   if (dir == (data[i] > data[j]))
   {
       SWAP(data[i], data[j]);
   }
}

//Sorts a bitonic sequence in ascending order if dir=1
//otherwise in descending order
void bitonicmerge_PAR(float *data,int low, int len, int dir)
{
   int k, i;   // k = split length
   if (len > 1)
   {
        k = len / 2;
        //#pragma omp parallel for //num_threads(2) ///(2) very poor but better than first try 164s                  //#pragma omp parallel for shared(data, dir, low, k) private(i) //(1) very poor 200s 64D100000P400Q
        for (i = low;i < low+k ;i++)
           compare_PAR(data, i, i+k, dir);
           bitonicmerge_PAR(data,low, k, dir);
           bitonicmerge_PAR(data,low+k, k, dir);
   }
}

//Generates bitonic sequence by sorting recursively
//two halves of the array in opposite sorting orders
//bitonicmerge will merge the resultant data
void recbitonic_PAR(float *data, int low, int len,int dir)
{
   int up = 1;
   int down = 0;

   int k;
   if (len > 1)
   {
       k = len / 2;
       recbitonic_PAR(data,low, k, up);
       recbitonic_PAR(data,low + k, k, down);
       bitonicmerge_PAR(data,low, len, dir);
   }
}

//Sorts the entire array
void bitonic_Sort_PAR(float *data,int len)
{
   int up = 1;

   recbitonic_PAR(data,0, len,up);
}




//################# MISC #################
//########################################
//Display arrays
void display_Array (float **arr,int m, int n){
  for (size_t i = 0; i < m; i++) {
    printf("\n\nq: %lu\n",i+1);
    for (size_t j = 0; j < n; j++) {
      printf("%f \t",*(*(arr +i)+j));
    }
  }
}

void display_Array_3D (float *arr,int m, int n,int dim){ // this is not a solution to an arbitrarily large matrix
                                                  // however it does demonstrate that an array can be passed to a function
  for (size_t i = 0; i < m; i++) {
    printf("\n\nq: %lu\n\n",i+1);
    for (size_t j = 0; j < n; j++) {
      printf("\np: %lu\t",j+1);
      for (size_t d = 0; d < dim; d++) {
        printf("%f  ",*(arr + i*n*dim + j*n + d));

      }
    }
  }
}
