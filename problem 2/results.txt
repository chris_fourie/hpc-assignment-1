###################################################
############### *Kronecker product* ###############
###################################################
i = 50
j = 50
k = 50
l = 50

calculating...


*Kronecker product*


*Expected dimensions* 2500 x 2500

Elapse time in SERIAL: 0.031250
Elapse time in PARALLEL: 0.023438


###################################################
i = 150
j = 150
k = 150
l = 150

calculating...


*Kronecker product*


*Expected dimensions* 22500 x 22500

Elapse time in SERIAL: 2.046875
Elapse time in PARALLEL: 0.947266


###################################################
i = 200
j = 200
k = 200
l = 200

calculating...


*Kronecker product*


*Expected dimensions* 40000 x 40000


Elapse time in SERIAL: 6.386719
Elapse time in PARALLEL: 3.068359


###################################################
############### *Khatri-Rao product* ##############
###################################################
Testing VALUES variation:1



i = 200
j = 200
k = 200
l = 200

calculating...


*Khatri-Rao product*


*Expected dimensions* 40000 x 200


Elapse time in SERIAL: 0.136719
Elapse time in PARALLEL: 0.097656

###################################################
Testing VALUES variation:2
i = 500
j = 500
k = 500
l = 500

calculating...


*Khatri-Rao product*


*Expected dimensions* 250000 x 500

Elapse time in SERIAL: 2.416016
Elapse time in PARALLEL: 1.179688



###################################################
Testing VALUES variation:3
i = 1000
j = 1000
k = 1000
l = 1000

calculating...


*Khatri-Rao product*


*Expected dimensions* 1000000 x 1000


Elapse time in SERIAL: 21.291016
Elapse time in PARALLEL: 9.994141
